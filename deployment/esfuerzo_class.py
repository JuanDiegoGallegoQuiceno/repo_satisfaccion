import sys

import re
import nltk
import spacy
import pandas as pd
import numpy as np
from unidecode import unidecode

from collections import Counter

from datetime import datetime

import pickle

class Effort:
    
    def __init__(self):

        path = 'D:\DataLab\datasets\CHAT\CustomerExperience'

        print("Loading Effort class..")

        # load core classes from libraries
        self.nlp = spacy.load('es_core_news_md')
        # set core regex
        self.re_agente = re.compile(r"agent: \)(.*?)\(")
        self.re_cliente = re.compile(r"customer: \)(.*?)\(")
        # load prdiction models
        self.scaler = pickle.load(open(path+'\ESFUERZO\MODELOS\esfuerzo_scaler.pkl', 'rb'))
        self.predict_effort = pickle.load(open(path+'\ESFUERZO\MODELOS\esfuerzo_KMeans_6_clust.pkl', 'rb'))

        print("Effort Class loaded!")


    def dialogs_preprocess(self, dialog, preprocess_on='customer'):
        '''
        function to preprocess the dialog before getting the numeric variables from text
        args:
        dialog: str. CHEC CONTACT-CENTER dialog in the form: ( agent: ) ______ ( customer: ) ____
        on: {'customer', 'agent'} apply preprocess on cutomer's or agent's lines
        '''

        dialog_in = dialog.lower()                                      #lowercase string
        dialog_in = unidecode(dialog_in)                                # remove accents and special characters

        agent = self.re_agente.findall(dialog)                          # get list with agent's interaction
        customer = self.re_cliente.findall(dialog)                      # get list with user's interaction

        if len(agent) == 0 or len(customer)==0:                         # return None if agent or customer said nothing
            return None

        else:
            if preprocess_on=='agent':
                                                                           # get rid of noise in agent's lines
                for i, dialog in enumerate(agent):

                    try:

                        first_item = agent[0]
                        second_item = agent[1]
                        if 'bienvenido' or 'buen' in dialog:
                            del agent[0]
                        elif 'buen' in second_item:
                            del agent[1]
                        
                        last_item = agent[-1]
                        before_last_item = agent[-2]
                        if 'http' in last_item:
                            del agent[-1]
                        if 'http' in before_last_item:
                            del agent[-2]
                        if 'desconectada' in last_item:
                            del agent[-1]
                        if 'hasta pronto' in before_last_item:
                            del agent[-2]
                        if 'no hemos recibido' in before_last_item:
                            del agent[-2]
                        if 'no hemos recibido' in last_item:
                            del agent[-1]

                    except IndexError:

                        pass

                agent_out = ' '.join(re.findall(r'[a-z]{2,}', ' '.join(agent)))

                return agent_out

            elif preprocess_on=='customer':

                for i, dialog in enumerate(customer):

                    try:

                        first_item = customer[0]
                        last_item = customer[-1]
                        if  'buen' in first_item:
                            del customer[0]
                        elif 'http' in last_item:
                            del customer[1]

                    except IndexError:
                        
                        pass
                
                customer_out = ' '.join(re.findall(r'[a-z]{2,}', ' '.join(customer)))

                return customer_out


    def get_word_count(self, preprocessed_dialog):
        '''
        function to get number of words from the agent or customer
        args:
        preprocessed_dialog: str. preprocessed interaction from agent or customer.
        count_on: {'customer', 'agent'}. get word count on agent's or customer's interaction. 
        '''

        word_count = len(preprocessed_dialog.split())

        return word_count


    def bigrams_frequency(self, preprocessed_dialog):
        '''
        function to get bigram count
        args:
        dialog: str. customer or agent preprocessed text
        '''

        
        bigrams = list(nltk.bigrams(preprocessed_dialog.split()))
        bigrams_freq = Counter(bigrams)

        bigram_count = 0
        for k, v in bigrams_freq.items():

            if v > 1:

                bigram_count = bigram_count + v
    
        return bigram_count


    def get_nouns_count(self, preprocessed_dialog):
        '''
        function to get nouns count.
        args:
        preprocessed_dialog: str. preprocessed interaction from agent or customer.
        '''

        doc = self.nlp(preprocessed_dialog)
        nouns = []
        for token in doc:
            if token.tag_ == 'NOUN':
                nouns.append(token.text)

        noun_count = 0
        nouns_freq = Counter(nouns)
        for k, v in nouns_freq.items():
            if v > 1:
                noun_count = noun_count + v
        
        return noun_count


    def conversation_time(self, event_time):
        '''
        function to get total interaction time
        args:
        event_time: dict. dictionary with timestamps records per interaction.
        '''
        init = event_time[0]
        init = list(init.values())[0]
        time_init = datetime.strptime(init, "%Y-%m-%d %H:%M:%S")
        end = event_time[-1]
        end = list(end.values())[0]
        time_end = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")

        res = time_end - time_init
        total_minutes = round(res.total_seconds()/60, 2)

        return total_minutes


    def get_effort(self, feature_vector):
        '''
        function to get effort level based on a feature vector
        '''
        scaled_vec = self.scaler.transform(np.reshape(feature_vector, (1, -1)))
        
        prediction = self.predict_effort.predict([scaled_vec.ravel()])

        labels =  {0: "Esfuerzo_bajo",
                    1: "Esfuerzo_alto",
                    2: "Esfuerzo_medio",
                    3: "Esfuerzo_bajo",
                    4: "Esfuerzo_alto",
                    5: "Esfuerzo_medio"}
        
        return labels[prediction[0]]