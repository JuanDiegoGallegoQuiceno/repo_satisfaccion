# system libraries
import os
import sys
import pickle

# crear metodo para hacer estos appends
sys.path.append('D:\DataLab\datasets\modules')
sys.path.append('D:\DataLab\datasets\CHAT\CustomerExperience')
sys.path.append('..')

# data handling
import re
import numpy as np

# NLP
from _text_normalization import TextPreprocess

# ML
from gensim.models import KeyedVectors
from sklearn.decomposition import PCA
from scipy.spatial import distance

class Satisfaction:

    def __init__(self):

        path = 'D:\DataLab\datasets\CHAT\CustomerExperience'

        print("Loading Satisfaction class...")

        # define core preprocess funtions
        self.TP = TextPreprocess()

        # load w2v core vectors
        print('Loading w2v vectors, it takes a while...')
        self.word_vectors = KeyedVectors.load_word2vec_format('D:\DataLab\datasets\CHAT\Pre-trained Word Vectors for Spanish\SBW-vectors-300-min5.txt')
        if self.word_vectors.vectors.shape[0]>0:
            print('w2v vectors loaded!')
        else: 
            print('w2v vectors did not load, process could not continue.')
            sys.exit()

        # load spase matrixes
        self.neg_word_embeddings = np.load(path+'\SATISFACCION\satisfaction_matrixes\/neg_satisfaction_matrix.npy')
        self.pos_word_embeddings = np.load(path+'\SATISFACCION\satisfaction_matrixes\pos_satisfaction_matrix.npy')
        self.neu_word_embeddings_1 = np.load(path+'\SATISFACCION\satisfaction_matrixes\/neu_satisfaction_matrix_1.npy')
        self.neu_word_embeddings_2 = np.load(path+'\SATISFACCION\satisfaction_matrixes\/neu_satisfaction_matrix_2.npy')

        # get mass center for matrixes
        self.cg_1 = np.mean(self.neg_word_embeddings, axis=0)
        self.cg_2 = np.mean(self.pos_word_embeddings, axis=0)
        self.cg_3 = np.mean(self.neu_word_embeddings_1, axis=0)
        self.cg_4 = np.mean(self.neu_word_embeddings_2, axis=0)
        self.cg = np.array([self.cg_1, self.cg_2, self.cg_3, self.cg_4])

        # load LogisticRegression trained for satisfaction
        self.lr_classifier = pickle.load(open(path+'\SATISFACCION\single_word_classifier\satisfaccion_LR.sav', 'rb'))

        # load PCA
        self.pca = PCA(n_components=2)

        print("Satisfaction class loaded!")


    def get_w2v_matrix(self, dialog, customer_dialog=True):
        '''
        function to get a matrix for a text on word2vec
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        customer_dialog: {bool}. get w2v matrix for customer. False get w2v matrix for agent
        '''

        re_agente = re.compile(r"agent: \)(.*?)\(")
        re_cliente = re.compile(r"customer: \)(.*?)\(") 

        if customer_dialog:
            text = (re.sub(r'\s{2,}', ' ', ' '.join(re_cliente.findall(dialog)))).strip()
            text = text.replace('http://servicio.asistenciachat.com/website/chec_chat/encuestam.aspx', '')
        else:
            text = (re.sub(r'\s{2,}', ' ', ' '.join(re_agente.findall(dialog)))).strip()
            text = text.replace('http://servicio.asistenciachat.com/website/chec_chat/encuestam.aspx', '')

        
        test = self.TP.text_normalization(text)                    # preprocesamiento general
        test = self.TP.entities_removal(text)                    # eliminacion de nombres propios
        test = self.TP.names_removal(text)                 # se limpia el texto, dejando unicamente letras

        # se definen las dimensiones de la matriz
        v = len(test.split())
        d = 300

        text_matrix = np.zeros((v, d))

        word_list = []

        for index, word in enumerate(test.split()):
            try:
                text_matrix[index] = self.word_vectors.get_vector(word)
                word_list.append(word)
            except KeyError:
                text_matrix[index] = np.zeros((1, 300)) # si no encuentra la palabra en los vectores, añada una fila de ceros
                pass

        return text_matrix, word_list


    def get_satisfaction_w2v(self, dialog):
        '''
        Function to get satisfaction level based on w2v distances method
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        FYI: args order in temp vector [neg, pos, neu, neu]
        '''
        text_matrix, word_list = self.get_w2v_matrix(dialog)
    
        try:
            text_matrix_embedding = self.pca.fit_transform(text_matrix)                      # machetazo
        except ValueError:
            return 'texto_nulo_o_insuficiente'

        text_embedding_NO_outliers = text_matrix_embedding[  
                            np.logical_and(text_matrix_embedding[:,0] > -0.2, text_matrix_embedding[:,1] < 0.5)
                        ]

        total_words = len(text_matrix_embedding) - len(text_embedding_NO_outliers)

        sp = 0
        sn = 0
        snu = 0

        for word in text_embedding_NO_outliers:
            temp_vector = np.zeros((4))
            for idx, center in enumerate(self.cg):
                temp_vector[idx] = distance.euclidean(word, center)
                
            if np.argmin(temp_vector) == 0:
                sn += 1
            elif np.argmin(temp_vector) == 1:
                sp += 1
            else:
                snu += 1

        if sn/text_embedding_NO_outliers.shape[0] > 0.60:
            satisfaccion = 'satisfaccion_negativa'
        elif sp/text_embedding_NO_outliers.shape[0] > 0.60:
            satisfaccion = 'satisfaccion_positiva'
        elif snu/text_embedding_NO_outliers.shape[0] > 0.60:
            satisfaccion = 'satisfaccion_neutra'
        else:
            satisfaccion = 'satisfaccion_neutra'
        
        return satisfaccion


    def get_satisfaccion_lr(self, dialog):
        '''
        Function to get satisfaction level based on logistic regression classifier.
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        '''

        text_matrix, word_list = self.get_w2v_matrix(dialog)

        if len(word_list)<1:
            return 'texto_nulo_o_insuficiente'

        temp_vector = np.zeros((3))
        dialog_len = text_matrix.shape[0]

        for row in text_matrix:
            temp_vector[0] += self.lr_classifier.predict_proba([row])[0][0]
            temp_vector[1] += self.lr_classifier.predict_proba([row])[0][1]
            temp_vector[2] += self.lr_classifier.predict_proba([row])[0][2]

        arg_max = np.argmax(temp_vector)
        if arg_max == 0 and temp_vector[0] > 0.60:
            return 'satisfaccion_negativa'
        elif arg_max == 1 and temp_vector[1] > 0.60:
            return 'satisfaccion_positiva'
        elif arg_max == 2 and temp_vector[2] > 0.60:
            return 'satisfaccion_neutra'
        else:
            return 'satisfaccion_neutra'


    def get_final_satisfaccion(self, dialog):
        '''
        This is the function to get the final satisfaction level. 
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        '''
        
        satisfaction_w2v = self.get_satisfaction_w2v(dialog)
        satisfaction_lr = self.get_satisfaccion_lr(dialog)
        
        if satisfaction_w2v=='satisfaccion_positiva' and satisfaction_lr=='satisfaccion_negativa':
            final_satisfaction = 'satisfaccion_neutra'
        elif satisfaction_w2v=='satisfaccion_negativa' and satisfaction_lr=='satisfaccion_positiva':
            final_satisfaction = 'satisfaccion_neutra'
        elif satisfaction_w2v=='satisfaccion_positiva' or satisfaction_w2v=='satisfaccion_negativa' and satisfaction_lr=='satisfaccion_neutra':
            final_satisfaction = satisfaction_w2v
        elif satisfaction_lr=='satisfaccion_positiva' or satisfaction_lr=='satisfaccion_negativa' and satisfaction_w2v=='satisfaccion_neutra':
            final_satisfaction = satisfaction_lr
        else:
            final_satisfaction = 'satisfaccion_neutra'
            
        return final_satisfaction

