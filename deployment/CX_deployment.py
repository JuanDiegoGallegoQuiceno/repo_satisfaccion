# system libraries
import os
import sys
import json
import pickle
import joblib

# crear modulo para estos appends
sys.path.append('D:\DataLab\datasets\modules')
sys.path.append('D:\DataLab\datasets\CHAT\CustomerExperience')
sys.path.append("D:\DataLab\datasets\CHAT\CustomerExperience\DEPLOYMENT")
sys.path.append("..")

# data handling
import re
import unidecode
import pandas as pd
import numpy as np

# CX
#from satisfaccion_class import Satisfaction
#from esfuerzo_class import Effort
from customer_experience_class import CustomerExperience, Satisfaction, Effort

# NLP
from nltk import word_tokenize
from _text_normalization import TextPreprocess

# DB
import pymongo
from pymongo import MongoClient

# visualizations
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.patches as mpatches
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator

# ML
import gensim
from sklearn.feature_extraction.text import TfidfVectorizer
from gensim.models import KeyedVectors
from sklearn.decomposition import PCA
from scipy.spatial import distance
import lightgbm as lgb

# Deep
from pysentimiento import SentimentAnalyzer
from pysentimiento import EmotionAnalyzer


def __main__():

    # load overall CX classes
    TP = TextPreprocess()
    CX = CustomerExperience()
    ST = Satisfaction()
    E = Effort()

    # set conection to DB
    client = MongoClient('mongodb://VOC:nTR9mDJdivcsR_@chec-bdpm01:27017/?authSource=VOC&readPreference=primary&appname=MongoDB%20Compass%20Readonly&ssl=false')
    db = client['VOC']

    # get records for the time period set
    since = '2021-08-01'
    until = '2021-08-31'

    records = db.Chat.find(filter={
    'fecha_evento':{
        '$gte':since,
        '$lt':until
        }
    })
    
    # create a df for work with structure data.
    chat_ids = []
    fec_inic = []
    fec_even = []
    even_ori = []
    chat_dlg = []

    for record in records:
        chat_ids.append(record['identificador_chat'])
        fec_inic.append(record['fecha_inicial'])
        fec_even.append(record['fecha_evento'])
        even_ori.append(record['origen_evento'])
        chat_dlg.append(record['dialogo_chat'])

    df = pd.DataFrame({'identificador_chat':chat_ids,
                    'fecha_inicial':fec_inic,
                    'fecha_evento':fec_even,
                    'origen_evento':even_ori,
                    'dialogo_chat':chat_dlg})

    ### preprocess the raw data got from the DB
    # get the dates to work with the format used
    event_dates = []
    for _id in df['identificador_chat'].unique():
        temp_df = df[df['identificador_chat']==_id]
        full_fecha_evento = []
        for idx, date in enumerate(temp_df['fecha_evento']):
            origen = temp_df['origen_evento'].iloc[idx]
            full_fecha_evento.append({origen:date})
        event_dates.append(full_fecha_evento)

    # get dialog to work with the format used.
    dialogs = []
    for _id in df['identificador_chat'].unique():
        temp_df = df[df['identificador_chat']==_id]
        full_dialog = []
        for idx, dialog in enumerate(temp_df['dialogo_chat']):
            if idx == 0:
                full_dialog.append(dialog)
            else:
                full_dialog.append(f"( {temp_df['origen_evento'].iloc[idx]}: ) {dialog}")

        dialogs.append(' '.join(full_dialog))
    
    # drop duplicates to set a new df cleaned
    df_use = df.drop_duplicates(subset=['identificador_chat'])
    del df_use['origen_evento']
    df_use['fecha_evento'] = event_dates
    df_use['dialogo_chat'] = dialogs

    # add one customer dialog column
    df_use['customer'] = df_use.dialogo_chat.map(lambda x: CX.get_customer_dialog(x))

    ### start assigning topic, sentiment, emotions, satisfaction and effort

    # assing topic
    df_use['topico'] = df_use.dialogo_chat.map(lambda x: CX.get_topic(x))

    # clean up non valuable predictions
    df_use.reset_index(drop=True, inplace=True)
    idx_drop = [i for i, topico in enumerate(df_use.topico) if topico == 'texto_nulo_o_insuficiente']
    df_use.drop(idx_drop, inplace=True)
    df_use.reset_index(drop=True, inplace=True)

    # assign sentiment
    df_use['sentimiento'] = df_use.dialogo_chat.map(lambda x: CX.get_sentimiento(x))

    # assign emotion
    df_use['emocion'] = df_use.dialogo_chat.map(lambda x: CX.get_emotion_PFA(x))

    # assign w2v satisfaction
    df_use['satisfaccion_w2v'] = df_use.dialogo_chat.map(lambda x: ST.get_satisfaction_w2v(x))

    # assign LR satisfaction
    df_use['satisfaccion_lr'] = df_use.dialogo_chat.map(lambda x: ST.get_satisfaccion_lr(x))

    # assign final satisfaction level
    df_use['satisfaccion'] = df_use.dialogo_chat.map(lambda x: ST.get_final_satisfaccion(x))

    # get features for Effort prediction
    agent_preprocessed = [E.dialogs_preprocess(dialog, preprocess_on='agent') for dialog in df_use.dialogo_chat]
    customer_preprocessed = [E.dialogs_preprocess(dialog, preprocess_on='customer') for dialog in df_use.dialogo_chat]

    agent_num_words = []
    customer_num_words = []
    bigrams_rep = []
    nouns_rep = []
    chat_total_time = []

    for dialog in agent_preprocessed:
        try:
            agent_num_words.append(E.get_word_count(dialog))
        except:
            agent_num_words.append(None)

    for dialog in customer_preprocessed:
        try:
            customer_num_words.append(E.get_word_count(dialog))
            bigrams_rep.append(E.bigrams_frequency(dialog))
            nouns_rep.append(E.get_nouns_count(dialog))
        except:
            customer_num_words.append(None)
            bigrams_rep.append(None)
            nouns_rep.append(None)
            
    for time_lst in df_use.fecha_evento:
        try:
            chat_total_time.append(E.conversation_time(time_lst)) 
        except:
            chat_total_time.append(None)

    df_use['agent_num_words'] = agent_num_words
    df_use['customer_num_words'] = customer_num_words
    df_use['bigrams_rep'] = bigrams_rep
    df_use['nouns_rep'] = nouns_rep
    df_use['chat_total_time'] = chat_total_time

    df_use.dropna(inplace=True)

    df_use['words_ratio'] = df_use.customer_num_words / df_use.agent_num_words
    df_use['words_per_minute_agent'] = df_use.agent_num_words / df_use.chat_total_time
    df_use['words_per_minute_customer'] = df_use.customer_num_words / df_use.chat_total_time

    # predict effort
    fet_vecs = df_use[['agent_num_words',
                'customer_num_words',
                'words_ratio',
                'bigrams_rep',
                'nouns_rep',
                'chat_total_time',
                'words_per_minute_agent',
                'words_per_minute_customer']].values

    predictions = [E.get_effort(row) for row in fet_vecs]
    df_use['esfuerzo'] = predictions

    ########## Saving outputs
    # save metadata for the inform
    out_path = f"D:\DataLab\datasets\CHAT\CustomerExperience\DEPLOYEMENT\deployment_outputs\CX_{since}_{until}"

    print("Beggining outputs saving stage...")

    # create a directory corresponding to the date range
    os.mkdir(path=out_path)

    df_metadata = {'since':since,
                    'until':until,
                    'obs_num':df_use.shape[0]
    }
    json.dump(df_metadata, open(out_path+f"\metadata_{since}_{until}.json", 'w'))

    # save the detail into an excel file
    df_use.to_excel(out_path+f"\IndicadoresCX_{since}_{until}.xlsx", index=False)

    ### visualizations
    satisfaction_df = pd.DataFrame({'nivel':[k for k, v in dict(df_use.groupby('satisfaccion').count()['identificador_chat']).items()],
                                'cantidad':[v for k, v in dict(df_use.groupby('satisfaccion').count()['identificador_chat']).items()]})

    esfuerzo_df = pd.DataFrame({'nivel':[k for k, v in dict(df_use.groupby('esfuerzo').count()['identificador_chat']).items()],
                                'cantidad':[v for k, v in dict(df_use.groupby('esfuerzo').count()['identificador_chat']).items()]})

    # por temas 
    topic_d = dict(df_use.groupby(['topico', 'satisfaccion']).count()['identificador_chat'])
    topics = df_use.topico.unique()
    stftn = df_use.satisfaccion.unique()
    esfz = df_use.esfuerzo.unique()

    satisfaction_tema_df = pd.DataFrame()
    satisfaction_tema_df['tema'] = topics
    satisfaction_tema_df['satisfaccion_neutra'] = ['not_set']*satisfaction_tema_df.shape[0]
    satisfaction_tema_df['satisfaccion_negativa'] = ['not_set']*satisfaction_tema_df.shape[0]
    satisfaction_tema_df['satisfaccion_positiva'] = ['not_set']*satisfaction_tema_df.shape[0]

    for i, t in enumerate(topics):
        s_t = []
        for s in stftn:
            try:
                v = topic_d[(t, s)]
            except KeyError:
                v = 0
            s_t.append(v)
        satisfaction_tema_df.iloc[i, 1:] = s_t

        
    topic_e = dict(df_use.groupby(['topico', 'esfuerzo']).count()['identificador_chat'])
    esfuerzo_tema_df = pd.DataFrame()
    esfuerzo_tema_df['tema'] = topics
    esfuerzo_tema_df['Esfuerzo_medio'] = ['not_set']*esfuerzo_tema_df.shape[0]
    esfuerzo_tema_df['Esfuerzo_bajo'] = ['not_set']*esfuerzo_tema_df.shape[0]
    esfuerzo_tema_df['Esfuerzo_alto'] = ['not_set']*esfuerzo_tema_df.shape[0]

    for i, t in enumerate(topics):
        e_t = []
        for e in esfz:
            try:
                v = topic_e[(t, e)]
            except KeyError:
                v = 0
            e_t.append(v)
        esfuerzo_tema_df.iloc[i, 1:] = e_t


    # pie chart satisfaction
    colors = sns.color_palette('pastel')

    def func(pct, allvalues):
        absolute = int(pct / 100.*np.sum(allvalues))
        return "{:1.1f}%\n({:d})".format(pct, absolute)

    fig, ax = plt.subplots(figsize=(8.5,5))

    wedges, texts, autotexts = ax.pie(satisfaction_df['cantidad'], labels=satisfaction_df.nivel,
                        autopct=lambda pct: func(pct, satisfaction_df['cantidad']), textprops={'fontsize':14},
                                    colors=colors, startangle=90)

    plt.setp(autotexts, size = 6, weight ="bold")
    ax.set_title("Proporción para satisfacción")
    plt.savefig(out_path+f'\satisfaccion_{since}_{until}.png')
    print("Satisfaccion pie chart saved!")

    # wordcloud low satisfaction
    text_low_satisfaction = ' '.join([text for text in df_use[df_use['satisfaccion']=='satisfaccion_negativa'].customer])
    text_low_satisfaction = TP.text_normalization(text_low_satisfaction, remove_accent=False)
    wordcloud = WordCloud(width=1000,
                        height=500,
                        max_font_size=150,
                        max_words=50,
                        collocations=False, 
                        background_color="white")
    WC_plot = wordcloud.generate(text_low_satisfaction)
    plt.figure()
    plt.grid(None)
    plt.imshow(WC_plot, interpolation="bilinear")
    plt.axis('off')
    plt.savefig(out_path+f'\satisfaccion_WC_{since}_{until}.png')
    print("Satisfaccion wordcloud saved!")

    # pie chart effort
    fig, ax = plt.subplots(figsize=(8.5,5))

    wedges, texts, autotexts = ax.pie(esfuerzo_df['cantidad'], labels=esfuerzo_df.nivel,
                        autopct=lambda pct: func(pct, esfuerzo_df['cantidad']), textprops={'fontsize':14},
                                    colors=colors, startangle=90)


    plt.setp(autotexts, size = 6, weight ="bold")
    ax.set_title("Proporción para esfuerzo")
    plt.savefig(out_path+f'\esfuerzo_{since}_{until}.png')
    print("Esfuerzo pie chart saved!")

    # wordcloud high effort
    text_high_effort = ' '.join([text for text in df_use[df_use['esfuerzo']=='Esfuerzo_alto'].customer])
    text_high_effort = TP.text_normalization(text_high_effort, remove_accent=False)
    WC_plot = wordcloud.generate(text_high_effort)
    plt.figure()
    plt.imshow(WC_plot, interpolation="bilinear")
    plt.axis('off')
    plt.savefig(out_path+f'\esfuerzo_WC_{since}_{until}.png')
    print("Esfuerzo wordcloud saved!")

    # stacked bar satisfaction per topic
    for i in range(len(satisfaction_tema_df)):
        satisfaction_tema_df.iloc[i, 1:] = satisfaction_tema_df.iloc[i, 1:]/sum(satisfaction_tema_df.iloc[i, 1:])

    # set the figure size
    def show_values_on_bars(axs):
        def _show_on_single_plot(ax):        
            for p in ax.patches:
                _x = p.get_x() + p.get_width() / 2
                _y = p.get_y() + p.get_height() / 2
                value = '{:.2f}%'.format((p.get_height())*100)
                if value != '0.00%':
                    ax.text(_x, _y, value, ha="center") 
                else:
                    ax.text(_x, _y, '', ha="center")

        if isinstance(axs, np.ndarray):
            for idx, ax in np.ndenumerate(axs):
                _show_on_single_plot(ax)
        else:
            _show_on_single_plot(axs)

    fig, ax =  plt.subplots(figsize=(14.5,6))

    ax.bar(x=satisfaction_tema_df.tema,
            height=satisfaction_tema_df.satisfaccion_negativa.values,
            bottom=0,
            color=colors[0],
            label='Satisfacción Negativa')
    show_values_on_bars(ax)

    ax.bar(x=satisfaction_tema_df.tema,
            height=satisfaction_tema_df.satisfaccion_neutra.values,
            bottom=satisfaction_tema_df.satisfaccion_negativa.values,
            color=colors[1],
            label='Satisfacción Neutra')
    show_values_on_bars(ax)

    ax.bar(x=satisfaction_tema_df.tema,
            height=satisfaction_tema_df.satisfaccion_positiva.values,
            bottom=satisfaction_tema_df.satisfaccion_negativa.values+ \
                    satisfaction_tema_df.satisfaccion_neutra.values,
            color=colors[2],
            label='Satisfacción Positiva')
    show_values_on_bars(ax)

    ax.set_ylabel('Conteo')
    ax.set_xlabel('\n Tema')
    ax.set_title('Satisfacción por Tema')

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.grid(axis='y')
    ax.legend(bbox_to_anchor=(1.0,1.0))
    plt.savefig(out_path+f'\satisfaccion_tema_{since}_{until}.png', bbox_inches="tight")
    print("Satisfaccion satackedbar saved!")


    # stacked bar effort per topic
    for i in range(len(esfuerzo_tema_df)):
        esfuerzo_tema_df.iloc[i, 1:] = esfuerzo_tema_df.iloc[i, 1:]/sum(esfuerzo_tema_df.iloc[i, 1:])

    fig, ax =  plt.subplots(figsize=(14.5,6))

    ax.bar(x=esfuerzo_tema_df.tema,
            height=esfuerzo_tema_df.Esfuerzo_bajo.values,
            bottom=0,
            color=colors[0],
            label='Esfuerzo Bajo')
    show_values_on_bars(ax)

    ax.bar(x=esfuerzo_tema_df.tema,
            height=esfuerzo_tema_df.Esfuerzo_medio.values,
            bottom=esfuerzo_tema_df.Esfuerzo_bajo.values,
            color=colors[1],
            label='Esfuerzo Medio')
    show_values_on_bars(ax)

    ax.bar(x=esfuerzo_tema_df.tema,
            height=esfuerzo_tema_df.Esfuerzo_alto.values,
            bottom=esfuerzo_tema_df.Esfuerzo_bajo.values+ \
                    esfuerzo_tema_df.Esfuerzo_medio.values,
            color=colors[2],
            label='Esfuerzo Alto')
    show_values_on_bars(ax)

    ax.set_ylabel('Conteo')
    ax.set_xlabel('\n Tema')
    ax.set_title('Esfuerzo por Tema')

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.grid(axis='y')

    ax.legend(bbox_to_anchor=(1.0,1.0))
    plt.savefig(out_path+f'\esfuerzo_tema_{since}_{until}.png', bbox_inches="tight")
    print("Esfuerzo satackedbar saved!")


if __name__=='__main__':
    __main__()