import json
import pdfkit
import base64

def __main__():

    print("Attachment process starting...")

    # define date range this is going to be variable
    since = '2021-08-01'
    until = '2021-08-31'

    # define the global path where the CX process outsputs are
    path = f"D:\DataLab\datasets\CHAT\CustomerExperience\DEPLOYEMENT\deployment_outputs\CX_{since}_{until}"

    # read metadata file
    metadata = json.load(open(path+f"\metadata_{since}_{until}.json", 'r'))
    # from metadata get the number of observations processed
    obs_num = metadata['obs_num']

    # define visualizations paths
    satisfaccion_pie_path = path+f'\satisfaccion_{since}_{until}.png'
    with open(satisfaccion_pie_path, 'rb') as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_data = base64.b64encode(binary_file_data)
        satisfaccion_pie_path_b64 = "data:image/png;base64," + base64_encoded_data.decode('utf-8')
        binary_file.close()

    satisfaccion_stacked_path = path+f'\satisfaccion_tema_{since}_{until}.png'
    with open(satisfaccion_stacked_path, 'rb') as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_data = base64.b64encode(binary_file_data)
        satisfaccion_stacked_path_b64 = "data:image/png;base64," + base64_encoded_data.decode('utf-8')
        binary_file.close()

    satisfaccion_wc_path = path+f'\satisfaccion_WC_{since}_{until}.png'
    with open(satisfaccion_wc_path, 'rb') as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_data = base64.b64encode(binary_file_data)
        satisfaccion_wc_path_b64 = "data:image/png;base64," + base64_encoded_data.decode('utf-8')
        binary_file.close()

    esfuerzo_pie_path = path+f'\esfuerzo_{since}_{until}.png'
    with open(esfuerzo_pie_path, 'rb') as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_data = base64.b64encode(binary_file_data)
        esfuerzo_pie_path_b64 = "data:image/png;base64," + base64_encoded_data.decode('utf-8')
        binary_file.close()

    esfuerzo_stacked_path = path+f'\esfuerzo_tema_{since}_{until}.png'
    with open(esfuerzo_stacked_path, 'rb') as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_data = base64.b64encode(binary_file_data)
        esfuerzo_stacked_path_b64 = "data:image/png;base64," + base64_encoded_data.decode('utf-8')
        binary_file.close()

    esfuerzo_wc_path = path+f'\esfuerzo_WC_{since}_{until}.png'
    with open(esfuerzo_wc_path, 'rb') as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_data = base64.b64encode(binary_file_data)
        esfuerzo_wc_path_b64 = "data:image/png;base64," + base64_encoded_data.decode('utf-8')
        binary_file.close()

    # write html 
    html_text = f" \
        <!DOCTYPE html> \
        <html> \
        <head> \
        <meta charset='utf-8'> \
        <meta name='viewport' content='width=device-width, initial-scale=1.0'> \
        <title>CX mail template</title> \
        <link rel='stylesheet' href='https://stackedit.io/style.css' /> \
        </head> \
        <body> \
            <h1>Informe de Indicadores de Experiencia de Cliente Fuente Chat entre {since} y {until}.</h1> \
                <table> \
                    <thead> \
                        <tr> \
                            <th>Rango de Fecha</th> \
                            <th>Observaciones</th> \
                        </tr> \
                    </thead> \
                        <tbody> \
                            <tr> \
                                <td>{since} - {until}</td> \
                                <td>{obs_num}</td> \
                            </tr> \
                        </tbody> \
                </table> \
            <h1>Satisfacción.</h1> \
                <h3>Resumen.</h3> \
                    <p><img src= {satisfaccion_pie_path_b64} \
                    alt='Satisfaction Pie'></p> \
                <h3>Distribuciones por Tema.</h3> \
                    <p><img src={satisfaccion_stacked_path_b64} \
                    alt='Satisfaction stacked'></p> \
                <h3>Palabras relacionadas con Satisfacción Baja.</h3> \
                    <p><img src={satisfaccion_wc_path_b64} \
                    alt='Satisfaction wordcloud'></p> \
            <h1>Esfuerzo.</h1> \
                <h3>Resumen.</h3> \
                    <p><img src={esfuerzo_pie_path_b64} \
                    alt='Effort Pie'></p> \
                <h3>Distribuciones por Tema.</h3> \
                    <p><img src={esfuerzo_stacked_path_b64} \
                    alt='Effort stacked'></p> \
                <h3>Palabras relacionadas con Esfuerzo Alto.</h3> \
                    <p><img src={esfuerzo_wc_path_b64} \
                    alt='Effort wordcloud'></p> \
        <body> \
        <html> \
    "

    # save html
    with open(path+f"\html_{since}_{until}.html", 'w', encoding='utf8') as file:
        file.write(html_text)
    
    # convert html to pdf
    path_wkhtmltopdf = r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=path_wkhtmltopdf)
    options = {'page-size': 'Legal'}
    pdfkit.from_file(path+f"\html_{since}_{until}.html",
                    path+f"\CX_{since}_{until}.pdf",
                    options=options,
                    configuration=config)

    print("Attachment created!")

if __name__=="__main__":
    __main__()

