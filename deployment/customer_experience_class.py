# system libraries
import os
import sys
import json
import pickle
import joblib

# crear metodos para estos appends
sys.path.append('D:\DataLab\datasets\modules')
sys.path.append('D:\DataLab\datasets\CHAT\CustomerExperience')

# data handling
import re
import unidecode
import pandas as pd
import numpy as np
from collections import Counter
from unidecode import unidecode

# date time 
from datetime import datetime

# CX
from satisfaccion_class import Satisfaction
from esfuerzo_class import Effort

# NLP
import nltk
import spacy
from nltk import word_tokenize
from _text_normalization import TextPreprocess

# DB
import pymongo
from pymongo import MongoClient

# visualizations
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.patches as mpatches

# ML
import gensim
from sklearn.feature_extraction.text import TfidfVectorizer
from gensim.models import KeyedVectors
from sklearn.decomposition import PCA
from scipy.spatial import distance
import lightgbm as lgb

# Deep
from pysentimiento import SentimentAnalyzer
from pysentimiento import EmotionAnalyzer

print('Loading w2v vectors, it takes a while...')
word_vectors = gensim.models.KeyedVectors.load_word2vec_format("D:\DataLab\datasets\CHAT\Pre-trained Word Vectors for Spanish\SBW-vectors-300-min5.txt")
if word_vectors.vectors.shape[0]>0:
    print('w2v vectors loaded!')
else: 
    print('w2v vectors did not load, process could not continue.')
    sys.exit()

class CustomerExperience:

    def __init__(self, word_vectors=word_vectors):
        print("Loading Customer Experience class...")

        # load core preprocess function
        self.TP = TextPreprocess()

        # load topics model
        self.doc_vectors = gensim.models.doc2vec.Doc2Vec.load('D:\DataLab\datasets\CHAT\CustomerExperience\gensimDoc2Vec_customer_chats(cleaned).model')

        with open('D:\DataLab\datasets\CHAT\CustomerExperience\mf_words.txt', 'rb') as f:                   
            self.mf_words = f.read().decode('utf8')
        self.mf_words =  self.mf_words.split('\n')
        self.mf_words = [word.lower() for word in self.mf_words]
        self.tfidf = joblib.load('D:\DataLab\datasets\CHAT\CustomerExperience\KMeans_topicos\/vectorizer_topico3-5-8.pkl') 
        self.KMeans_10 = joblib.load('D:\DataLab\datasets\CHAT\CustomerExperience\KMeans_topicos\KMeans_10_chats.sav')                                                                
        self.KMeans_2 = joblib.load('D:\DataLab\datasets\CHAT\CustomerExperience\KMeans_topicos\KMeans_topic2.sav')
        self.LDA_3_5_8 = joblib.load('D:\DataLab\datasets\CHAT\CustomerExperience\KMeans_topicos\LDA_topico3-5-8.pkl')
        self.KMeans_6_9 = joblib.load('D:\DataLab\datasets\CHAT\CustomerExperience\KMeans_topicos\KMeans_indef_chats.sav')

        # load sentiment model
        self.analyzer = SentimentAnalyzer(lang='es')

        ## load model and data for emotion prediction
        #print('Loading w2v vectors, it takes a while...')
        #self.word_vectors = gensim.models.KeyedVectors.load_word2vec_format("D:\DataLab\datasets\CHAT\Pre-trained Word Vectors for Spanish\SBW-vectors-300-min5.txt")
        #if self.word_vectors.vectors.shape[0]>0:
        #    print('w2v vectors loaded!')
        #else: 
        #    print('w2v vectors did not load, process could not continue.')
        #    sys.exit()

        self.word_vectors = word_vectors

        self.feature_selector = joblib.load('D:\DataLab\datasets\CHAT\CustomerExperience\EMOCIONES\LinearSVC\SVD_80.pkl')      
        self.classifier = joblib.load('D:\DataLab\datasets\CHAT\CustomerExperience\EMOCIONES\LinearSVC\emotion_classifier.pkl')

        print("Customer Experience class loaded!")


    def get_customer_dialog(self, dialog):
        '''
        function to get the customer dialog.
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        '''

        re_cliente = re.compile(r"customer: \)(.*?)\(")

        text = (re.sub(r'\s{2,}', ' ', ' '.join(re_cliente.findall(dialog)))).strip()
        text_out = self.TP.text_normalization(text, remove_accent=False)
        text_out = self.TP.entities_removal(text_out)
        text_out = self.TP.names_removal(text_out)

        return text_out


    def remove_mf_words(self, text):
        '''
        function to remove the words from mf_words from any text
        args:
        text: any string to remove the words got with get_most_common_words()
        '''

        text = word_tokenize(text)
        for word in self.mf_words:
            while word in text:
                text.remove(word)
        return ' '.join(text)
    

    def get_topic(self, dialog):
        '''
        Function to get topic for chats.
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        '''
        re_cliente = re.compile(r"customer: \)(.*?)\(")
        text = (re.sub(r'\s{2,}', ' ', ' '.join(re_cliente.findall(dialog)))).strip()       
        text = re.sub(r'https://\S{2,}', '', text)                                          
        text = self.TP.text_normalization(text)                                                  
        text = self.remove_mf_words(text)                                                        
        text = self.TP.entities_removal(text)                                                    
        text = self.TP.names_removal(text)                                                       
        
        if len(text.split()) >= 5:
            vector = self.doc_vectors.infer_vector(text.split())                                     

            topic = self.KMeans_10.predict([vector])                                                 

            if topic == 0:
                topic_name = 'facturacion'
            elif topic == 1:
                topic_name = 'facturacion'
            elif topic == 2:
                topic_2 = self.KMeans_2.predict([vector])
                if topic_2 == 0:
                    topic_name = 'facturacion'
                elif topic_2 == 1:
                    topic_name = 'facturacion'
                else:
                    topic_name = 'facturacion'
            elif topic in [3,5,8]:
                M = self.tfidf.transform([text])
                topic_2 = (self.LDA_3_5_8.transform(M)).argmax(axis=1)[0]
                if topic_2 == 0:
                    topic_name = 'facturacion'
                elif topic_2 == 1:
                    topic_name = 'financiacion_pago_parcial'
                elif topic_2 == 2:
                    topic_name = 'facturacion'
                elif topic_2 == 3:
                    topic_name = 'pago_parcial'
                elif topic_2 == 4:
                    topic_name = 'facturacion'
            elif topic in [4, 7]:
                topic_name = 'inconformidad_consumo_factura'
            elif topic in [6,9]:
                topic_2 = self.KMeans_6_9.predict([vector])
                if topic_2 == 0:
                    topic_name = 'informacion_general'
                elif topic_2 == 1:
                    topic_name = 'inconformidad_consumo_factura'
                elif topic_2 == 2:
                    topic_name = 'inconformidad_consumo_factura'
                elif topic_2 == 3:
                    topic_name = 'facturacion'
                elif topic_2 == 4:
                    topic_name = 'informacion_general'
                elif topic_2 == 5:
                    topic_name = 'informacion_general'
        
        else:
            topic_name = 'texto_nulo_o_insuficiente'

        return topic_name


    def get_sentimiento(self, dialog, customer_dialog=True):
        '''
        Function to get the overall sentiment of a dialog
        args:
        dialog: text dialog in format ( agent: )______ ( customer: )_____
        customer_dialog: {bool}. ddefault True, gets sentiment for customer dialog part
        '''

        re_agente = re.compile(r"agent: \)(.*?)\(")
        re_cliente = re.compile(r"customer: \)(.*?)\(")

        if customer_dialog:
            customer = re_cliente.findall(dialog)
            customer = [self.TP.text_normalization(line) for line in customer]             # preprocesamiento con la función definida antes
            customer = [self.TP.names_removal(line) for line in customer]
            customer = [self.TP.entities_removal(line) for line in customer]
            customer = [line for line in customer if len(line)>=2]
            
            if len(customer) > 1:
                sents = np.zeros((3))

                customer_neg_sent = list(self.analyzer.predict(k).probas['NEG'] for k in customer)
                sents[0] = np.mean(customer_neg_sent)
                customer_pos_sent = list(self.analyzer.predict(k).probas['POS'] for k in customer)
                sents[1] = np.mean(customer_pos_sent)
                customer_neu_sent = list(self.analyzer.predict(k).probas['NEU'] for k in customer)
                sents[2] = np.mean(customer_neu_sent)

                arg_max = np.argmax(sents)

                # aplicacion de reglas heurísticas
                if arg_max == 0 and sents[0] > 0.60:
                    sentimiento = 'sentimiento_negativo'
                elif arg_max == 1 and sents[1] > 0.60:
                    sentimiento = 'sentimiento_positivo'
                elif arg_max == 2 and sents[2] > 0.60:
                    sentimiento = 'sentimiento_neutro'
                else:
                    sentimiento = 'sentimiento_neutro'
                
            else:
                sentimiento = 'texto_nulo_o_insuficiente'

        else:

            agent = re_agente.findall(dialog)
            agent = [self.TP.text_normalization(line) for line in agent]
            agent = [self.TP.entities_removal(line) for line in agent]
            agent = [self.TP.names_removal(line) for line in agent]
            
            sents = np.zeros((3))

            agent_neg_sent = list(self.analyzer.predict(k).probas['NEG'] for k in agent)
            sents[0] = np.mean(agent_neg_sent)
            agent_pos_sent = list(self.analyzer.predict(k).probas['POS'] for k in agent)
            sents[1] = np.mean(agent_pos_sent)
            agent_neu_sent = list(self.analyzer.predict(k).probas['NEU'] for k in agent)
            sents[2] = np.mean(agent_neu_sent)

            arg_max = np.argmax(sents)
            
            # aplicacion de reglas heurísticas
            if arg_max == 0 and sents[0] > 0.60:
                sentimiento = 'sentimiento_negativo'
            elif arg_max == 1 and sents[1] > 0.60:
                sentimiento = 'sentimiento_positivo'
            elif arg_max == 2 and sents[2] > 0.60:
                sentimiento = 'sentimiento_neutro'
            else:
                sentimiento = 'sentimiento_neutro'

        return sentimiento


    def get_emotion_PFA(self, dialog, customer_dialog=True):
        '''
        Function to get the overall emotion of a dialog
        args:
        dialog: text dialog in format ( agent: )______ ( customer: )_____
        customer_dialog: {bool}. ddefault True, gets sentiment for customer dialog part
        '''

        
        re_agente = re.compile(r"agent: \)(.*?)\(")
        re_cliente = re.compile(r"customer: \)(.*?)\(") 

        emo_dict = {0:'alegria', 1:'tristeza', 2:'enojo'}
        emo_array = np.zeros((3))

        if customer_dialog:
            customer = re_cliente.findall(dialog)
            customer = [self.TP.text_normalization(line) for line in customer]            
            customer = [self.TP.names_removal(line) for line in customer]
            customer = [self.TP.entities_removal(line) for line in customer]

            if len(customer)>1:
                M = np.zeros((len(customer), 300))
                for i, word in enumerate(customer):
                    try:
                        M[i] = self.word_vectors.get_vector(word)
                    except KeyError:
                        pass
            else:
                return 'texto_nulo_o_insuficiente'

        else:
            agent = re_cliente.findall(dialog)
            agent = [self.TP.text_normalization(line) for line in agent]            
            agent = [self.TP.names_removal(line) for line in agent]
            agent = [self.TP.entities_removal(line) for line in agent]

            M = np.zeros((len(agent), 300))
            for i, word in enumerate(agent):
                try:
                    M[i] = self.word_vectors.get_vector(word)
                except KeyError:
                    pass
        
        M_r = self.feature_selector.transform(M)
        for i in M_r:
            if self.classifier.predict([i])[0] == 'alegria':
                emo_array[0] += 1
            elif self.classifier.predict([i])[0] == 'tristeza':
                emo_array[1] += 1
            else:
                emo_array[2] += 1
                
        dialog_len = M_r.shape[0]
        emo_array = emo_array/dialog_len
        arg_max = np.argmax(emo_array)

        if arg_max == 0 and emo_array[0] > 0.60:
            return 'alegria'
        elif arg_max == 1 and emo_array[1] > 0.60:
            return 'tristeza'
        elif arg_max == 2 and emo_array[2] > 0.60:
            return 'enojo'
        else:
            return 'neutro'

class Satisfaction:

    def __init__(self, word_vectors=word_vectors):

        path = 'D:\DataLab\datasets\CHAT\CustomerExperience'

        print("Loading Satisfaction class...")

        # define core preprocess funtions
        self.TP = TextPreprocess()

        # load w2v core vectors
        #print('Loading w2v vectors, it takes a while...')
        #self.word_vectors = KeyedVectors.load_word2vec_format('D:\DataLab\datasets\CHAT\Pre-trained Word Vectors for Spanish\SBW-vectors-300-min5.txt')
        #if self.word_vectors.vectors.shape[0]>0:
        #    print('w2v vectors loaded!')
        #else: 
        #    print('w2v vectors did not load, process could not continue.')
        #    sys.exit()

        self.word_vectors = word_vectors

        # load spase matrixes
        self.neg_word_embeddings = np.load(path+'\SATISFACCION\satisfaction_matrixes\/neg_satisfaction_matrix.npy')
        self.pos_word_embeddings = np.load(path+'\SATISFACCION\satisfaction_matrixes\pos_satisfaction_matrix.npy')
        self.neu_word_embeddings_1 = np.load(path+'\SATISFACCION\satisfaction_matrixes\/neu_satisfaction_matrix_1.npy')
        self.neu_word_embeddings_2 = np.load(path+'\SATISFACCION\satisfaction_matrixes\/neu_satisfaction_matrix_2.npy')

        # get mass center for matrixes
        self.cg_1 = np.mean(self.neg_word_embeddings, axis=0)
        self.cg_2 = np.mean(self.pos_word_embeddings, axis=0)
        self.cg_3 = np.mean(self.neu_word_embeddings_1, axis=0)
        self.cg_4 = np.mean(self.neu_word_embeddings_2, axis=0)
        self.cg = np.array([self.cg_1, self.cg_2, self.cg_3, self.cg_4])

        # load LogisticRegression trained for satisfaction
        self.lr_classifier = pickle.load(open(path+'\SATISFACCION\single_word_classifier\satisfaccion_LR.sav', 'rb'))

        # load PCA
        self.pca = PCA(n_components=2)

        print("Satisfaction class loaded!")


    def get_w2v_matrix(self, dialog, customer_dialog=True):
        '''
        function to get a matrix for a text on word2vec
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        customer_dialog: {bool}. get w2v matrix for customer. False get w2v matrix for agent
        '''

        re_agente = re.compile(r"agent: \)(.*?)\(")
        re_cliente = re.compile(r"customer: \)(.*?)\(") 

        if customer_dialog:
            text = (re.sub(r'\s{2,}', ' ', ' '.join(re_cliente.findall(dialog)))).strip()
            text = text.replace('http://servicio.asistenciachat.com/website/chec_chat/encuestam.aspx', '')
        else:
            text = (re.sub(r'\s{2,}', ' ', ' '.join(re_agente.findall(dialog)))).strip()
            text = text.replace('http://servicio.asistenciachat.com/website/chec_chat/encuestam.aspx', '')

        
        test = self.TP.text_normalization(text)                    # preprocesamiento general
        test = self.TP.entities_removal(text)                    # eliminacion de nombres propios
        test = self.TP.names_removal(text)                 # se limpia el texto, dejando unicamente letras

        # se definen las dimensiones de la matriz
        v = len(test.split())
        d = 300

        text_matrix = np.zeros((v, d))

        word_list = []

        for index, word in enumerate(test.split()):
            try:
                text_matrix[index] = self.word_vectors.get_vector(word)
                word_list.append(word)
            except KeyError:
                text_matrix[index] = np.zeros((1, 300)) # si no encuentra la palabra en los vectores, añada una fila de ceros
                pass

        return text_matrix, word_list


    def get_satisfaction_w2v(self, dialog):
        '''
        Function to get satisfaction level based on w2v distances method
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        FYI: args order in temp vector [neg, pos, neu, neu]
        '''
        text_matrix, word_list = self.get_w2v_matrix(dialog)
    
        try:
            text_matrix_embedding = self.pca.fit_transform(text_matrix)                      # machetazo
        except ValueError:
            return 'texto_nulo_o_insuficiente'

        text_embedding_NO_outliers = text_matrix_embedding[  
                            np.logical_and(text_matrix_embedding[:,0] > -0.2, text_matrix_embedding[:,1] < 0.5)
                        ]

        total_words = len(text_matrix_embedding) - len(text_embedding_NO_outliers)

        sp = 0
        sn = 0
        snu = 0

        for word in text_embedding_NO_outliers:
            temp_vector = np.zeros((4))
            for idx, center in enumerate(self.cg):
                temp_vector[idx] = distance.euclidean(word, center)
                
            if np.argmin(temp_vector) == 0:
                sn += 1
            elif np.argmin(temp_vector) == 1:
                sp += 1
            else:
                snu += 1

        if sn/text_embedding_NO_outliers.shape[0] > 0.60:
            satisfaccion = 'satisfaccion_negativa'
        elif sp/text_embedding_NO_outliers.shape[0] > 0.60:
            satisfaccion = 'satisfaccion_positiva'
        elif snu/text_embedding_NO_outliers.shape[0] > 0.60:
            satisfaccion = 'satisfaccion_neutra'
        else:
            satisfaccion = 'satisfaccion_neutra'
        
        return satisfaccion


    def get_satisfaccion_lr(self, dialog):
        '''
        Function to get satisfaction level based on logistic regression classifier.
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        '''

        text_matrix, word_list = self.get_w2v_matrix(dialog)

        if len(word_list)<1:
            return 'texto_nulo_o_insuficiente'

        temp_vector = np.zeros((3))
        dialog_len = text_matrix.shape[0]

        for row in text_matrix:
            temp_vector[0] += self.lr_classifier.predict_proba([row])[0][0]
            temp_vector[1] += self.lr_classifier.predict_proba([row])[0][1]
            temp_vector[2] += self.lr_classifier.predict_proba([row])[0][2]

        arg_max = np.argmax(temp_vector)
        if arg_max == 0 and temp_vector[0] > 0.60:
            return 'satisfaccion_negativa'
        elif arg_max == 1 and temp_vector[1] > 0.60:
            return 'satisfaccion_positiva'
        elif arg_max == 2 and temp_vector[2] > 0.60:
            return 'satisfaccion_neutra'
        else:
            return 'satisfaccion_neutra'


    def get_final_satisfaccion(self, dialog):
        '''
        This is the function to get the final satisfaction level. 
        args:
        dialog: {str}. text dialog in format ( agent: )______ ( customer: )_____
        '''
        
        satisfaction_w2v = self.get_satisfaction_w2v(dialog)
        satisfaction_lr = self.get_satisfaccion_lr(dialog)
        
        if satisfaction_w2v=='satisfaccion_positiva' and satisfaction_lr=='satisfaccion_negativa':
            final_satisfaction = 'satisfaccion_neutra'
        elif satisfaction_w2v=='satisfaccion_negativa' and satisfaction_lr=='satisfaccion_positiva':
            final_satisfaction = 'satisfaccion_neutra'
        elif satisfaction_w2v=='satisfaccion_positiva' or satisfaction_w2v=='satisfaccion_negativa' and satisfaction_lr=='satisfaccion_neutra':
            final_satisfaction = satisfaction_w2v
        elif satisfaction_lr=='satisfaccion_positiva' or satisfaction_lr=='satisfaccion_negativa' and satisfaction_w2v=='satisfaccion_neutra':
            final_satisfaction = satisfaction_lr
        else:
            final_satisfaction = 'satisfaccion_neutra'
            
        return final_satisfaction

class Effort:
    
    def __init__(self):

        path = 'D:\DataLab\datasets\CHAT\CustomerExperience'

        print("Loading Effort class..")

        # load core classes from libraries
        self.nlp = spacy.load('es_core_news_md')
        # set core regex
        self.re_agente = re.compile(r"agent: \)(.*?)\(")
        self.re_cliente = re.compile(r"customer: \)(.*?)\(")
        # load prdiction models
        self.scaler = pickle.load(open(path+'\ESFUERZO\MODELOS\esfuerzo_scaler.pkl', 'rb'))
        self.predict_effort = pickle.load(open(path+'\ESFUERZO\MODELOS\esfuerzo_KMeans_6_clust.pkl', 'rb'))

        print("Effort Class loaded!")


    def dialogs_preprocess(self, dialog, preprocess_on='customer'):
        '''
        function to preprocess the dialog before getting the numeric variables from text
        args:
        dialog: str. CHEC CONTACT-CENTER dialog in the form: ( agent: ) ______ ( customer: ) ____
        on: {'customer', 'agent'} apply preprocess on cutomer's or agent's lines
        '''

        dialog_in = dialog.lower()                                      #lowercase string
        dialog_in = unidecode(dialog_in)                                # remove accents and special characters

        agent = self.re_agente.findall(dialog)                          # get list with agent's interaction
        customer = self.re_cliente.findall(dialog)                      # get list with user's interaction

        if len(agent) == 0 or len(customer)==0:                         # return None if agent or customer said nothing
            return None

        else:
            if preprocess_on=='agent':
                                                                           # get rid of noise in agent's lines
                for i, dialog in enumerate(agent):

                    try:

                        first_item = agent[0]
                        second_item = agent[1]
                        if 'bienvenido' or 'buen' in dialog:
                            del agent[0]
                        elif 'buen' in second_item:
                            del agent[1]
                        
                        last_item = agent[-1]
                        before_last_item = agent[-2]
                        if 'http' in last_item:
                            del agent[-1]
                        if 'http' in before_last_item:
                            del agent[-2]
                        if 'desconectada' in last_item:
                            del agent[-1]
                        if 'hasta pronto' in before_last_item:
                            del agent[-2]
                        if 'no hemos recibido' in before_last_item:
                            del agent[-2]
                        if 'no hemos recibido' in last_item:
                            del agent[-1]

                    except IndexError:

                        pass

                agent_out = ' '.join(re.findall(r'[a-z]{2,}', ' '.join(agent)))

                return agent_out

            elif preprocess_on=='customer':

                for i, dialog in enumerate(customer):

                    try:

                        first_item = customer[0]
                        last_item = customer[-1]
                        if  'buen' in first_item:
                            del customer[0]
                        elif 'http' in last_item:
                            del customer[1]

                    except IndexError:
                        
                        pass
                
                customer_out = ' '.join(re.findall(r'[a-z]{2,}', ' '.join(customer)))

                return customer_out


    def get_word_count(self, preprocessed_dialog):
        '''
        function to get number of words from the agent or customer
        args:
        preprocessed_dialog: str. preprocessed interaction from agent or customer.
        count_on: {'customer', 'agent'}. get word count on agent's or customer's interaction. 
        '''

        word_count = len(preprocessed_dialog.split())

        return word_count


    def bigrams_frequency(self, preprocessed_dialog):
        '''
        function to get bigram count
        args:
        dialog: str. customer or agent preprocessed text
        '''

        
        bigrams = list(nltk.bigrams(preprocessed_dialog.split()))
        bigrams_freq = Counter(bigrams)

        bigram_count = 0
        for k, v in bigrams_freq.items():

            if v > 1:

                bigram_count = bigram_count + v
    
        return bigram_count


    def get_nouns_count(self, preprocessed_dialog):
        '''
        function to get nouns count.
        args:
        preprocessed_dialog: str. preprocessed interaction from agent or customer.
        '''

        doc = self.nlp(preprocessed_dialog)
        nouns = []
        for token in doc:
            if token.tag_ == 'NOUN':
                nouns.append(token.text)

        noun_count = 0
        nouns_freq = Counter(nouns)
        for k, v in nouns_freq.items():
            if v > 1:
                noun_count = noun_count + v
        
        return noun_count


    def conversation_time(self, event_time):
        '''
        function to get total interaction time
        args:
        event_time: dict. dictionary with timestamps records per interaction.
        '''
        init = event_time[0]
        init = list(init.values())[0]
        time_init = datetime.strptime(init, "%Y-%m-%d %H:%M:%S")
        end = event_time[-1]
        end = list(end.values())[0]
        time_end = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")

        res = time_end - time_init
        total_minutes = round(res.total_seconds()/60, 2)

        return total_minutes


    def get_effort(self, feature_vector):
        '''
        function to get effort level based on a feature vector
        '''
        scaled_vec = self.scaler.transform(np.reshape(feature_vector, (1, -1)))
        
        prediction = self.predict_effort.predict([scaled_vec.ravel()])

        labels =  {0: "Esfuerzo_bajo",
                    1: "Esfuerzo_alto",
                    2: "Esfuerzo_medio",
                    3: "Esfuerzo_bajo",
                    4: "Esfuerzo_alto",
                    5: "Esfuerzo_medio"}
        
        return labels[prediction[0]]
