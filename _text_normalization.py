import re
import spacy
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk import word_tokenize

nlp = spacy.load('es_core_news_md')

def text_normalization(text, words_to_drop=None):
    '''
    function to return texts without numbers, punctuation and stopwords
    args:
    text: a string
    words_to_drop = list of additional words to delete from texts 
    '''
    stop_words = list(stopwords.words('spanish'))
    if words_to_drop is not None:
        for w in words_to_drop:
            stop_words.append(w)
    text = str(text)
    text = text.lower()
    text = ' '.join(re.findall(r'[a-záéíóúñ]{2,}', text))
    text = word_tokenize(text)
    for w in stop_words:
        while w in text: text.remove(w)
    return ' '.join(list(dict.fromkeys(text)))

